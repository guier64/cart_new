<?php

class User
{
    public $name;
    public $email;
    public $cart;

    /**
     * constructor
     * @param  string  $name
     * @param  string  $email
     * @param  Cart  $cart
     */
    public function __construct($name, $email, Cart $cart)
    {
        $this->name = $name;
        $this->email = $email;
        $this->cart = $cart;
    }
}
