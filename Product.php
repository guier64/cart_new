<?php

class Product
{
    public $name;
    public $price;
    public $qty;

    /**
     * constructor
     * @param  string  $name
     * @param  float  $price
     * @param  int  $qty
     */
    public function __construct($name, $price, $qty)
    {
        $this->name = $name;
        $this->price = $price;
        $this->qty = $qty;
    }
}
