<?php


class Cart
{
    protected $cart_contents;
    protected $total_price;


    /**
     * constructor
     */
    public function __construct()
    {
        $this->cart_contents = [];
        $this->total_price = 0;
    }




    public function update(Product $product)
    {
        if (!isset($product->name, $product->price, $product->qty)) {
            return false;
        } else {

            // prep the quantity
            $product->qty= (int) $product->qty;

            if ($product->qty == 0) {
                return false;
            }
            // prep the price
            $product->price = (float) $product->price;

            $product_name = $product->name;


            //check if prodcut already exits in cart
            $old_qty = isset($this->cart_contents[$product_name]->qty) ? (int) $this->cart_contents[$product_name]->qty : 0;

            $product->qty += $old_qty;

            //save prodcut in cart_contents array
            $this->cart_contents[$product_name] = $product;
        }
    }




    /**
     * Caculate the total price of cart
     * @return    float
     */
    public function total_price()
    {
        foreach ($this->cart_contents as $key => $val) {
            $this->total_price += $val->price * $val->qty;
        }

        return $this->total_price;
    }
}
