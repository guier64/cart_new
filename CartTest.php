<?php
require 'User.php';
require 'Product.php';
require 'Cart.php';






final class CartTests extends PHPUnit_Framework_TestCase
{
    public function testTotal_price()
    {
        $new_cart = new Cart();

        $product_1 = new Product('Apple', 4.95, 2);
        $product_2 = new Product('Orange', 3.99, 1);


        $new_user = new User('John Doe', 'john.doe@example.com', $new_cart);


        $new_cart->update($product_1);
        $new_cart->update($product_2);
            

        $result = $new_cart->total_price();

        $this->assertEquals(13.89, $result);
    }
    
    public function testAddandRemove()
    {
        $new_cart = new Cart();

        $product_1 = new Product('Apple', 4.95, 3);
        $product_2 = new Product('Apple', 4.95, -1);


        $new_user = new User('John Doe', 'john.doe@example.com', $new_cart);


        $new_cart->update($product_1);
        $new_cart->update($product_2);
            

        $result = $new_cart->total_price();

        $this->assertEquals(9.9, $result);
    }
}
